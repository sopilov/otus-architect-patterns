package ru.otus

data class Vector(
        val x: Int,
        val y: Int
) {
    operator fun plus(element: Vector): Vector {
        return Vector(x + element.x, y + element.y)
    }
}