package ru.otus

interface Command {
    fun execute()
}