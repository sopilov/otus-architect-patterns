package ru.otus

class RotateCommand(
        private val r: Rotable
): Command {
    override fun execute() {
        r.setDirection((r.getDirection() + r.getAngularVelocity()) % r.getMaxDirections())
    }
}