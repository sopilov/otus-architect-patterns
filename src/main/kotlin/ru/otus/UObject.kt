package ru.otus

interface UObject {
    fun getProperty(propertyName: String) : Any
    fun setProperty(propertyName: String, newValue: Any)
}