package ru.otus

import kotlin.math.abs
import kotlin.math.sqrt

class MathFunc {

    fun solve(a: Double, b: Double, c: Double): List<Double> {
        if (!a.isFinite() || !b.isFinite() || !c.isFinite())  throw IllegalArgumentException()
        val e = 1e-5
        if (abs(a) < e) throw IllegalArgumentException()
        val d = b * b - 4 * a * c

        if (abs(d) < e) return listOf(-b / (2 * a))
        else if (d > e) return listOf(-b + sqrt(d) / (2 * a), -b - sqrt(d) / (2 * a))
        else return emptyList()
    }
}