package ru.otus

class MovableAdapter(
        private val m: UObject
): Movable {
    override fun getPosition(): Vector {
        return m.getProperty("position") as Vector
    }

    override fun getVelocity(): Vector {
        return m.getProperty("velocity") as Vector
    }

    override fun setPosition(newValue: Vector) {
        m.setProperty("position", newValue)
    }
}