package ru.otus

interface Movable {
    fun getPosition() : Vector
    fun getVelocity() : Vector
    fun setPosition(newValue: Vector)
}