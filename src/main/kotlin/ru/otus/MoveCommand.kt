package ru.otus

class MoveCommand(
        val m: Movable
) : Command {
    override fun execute() {
        m.setPosition(m.getPosition() + m.getVelocity())
    }
}