package ru.otus

class RotableAdapter(
        private val r: UObject
): Rotable {
    override fun getDirection(): Int {
        return r.getProperty("direction") as Int
    }

    override fun getAngularVelocity(): Int {
        return r.getProperty("angularVelocity") as Int
    }

    override fun getMaxDirections(): Int {
        return r.getProperty("maxDirections") as Int
    }

    override fun setDirection(newValue: Int) {
        r.setProperty("direction", newValue)
    }
}