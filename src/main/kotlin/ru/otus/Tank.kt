package ru.otus

class Tank(
        val properties: MutableMap<String, Any> = mutableMapOf()
): UObject {
    override fun getProperty(propertyName: String): Any {
        return properties.getValue(propertyName)
    }

    override fun setProperty(propertyName: String, newValue: Any) {
        if (!properties.containsKey(propertyName)) throw RuntimeException("Tank hasn't property $propertyName")
        properties[propertyName] = newValue
    }
}