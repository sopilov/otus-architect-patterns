package ru.otus

interface Rotable {
    fun getDirection(): Int
    fun getAngularVelocity(): Int
    fun getMaxDirections(): Int
    fun setDirection(newValue: Int)
}