package ru.otus

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.lang.IllegalArgumentException

internal class MathFuncTest {
    val mathFunc = MathFunc()

    @Test
    fun solve() {
        //для уравнения x^2+1 = 0 корней нет
        assert(mathFunc.solve(1.0, 0.0, 1.0).isEmpty())

        //для уравнения x^2-1 = 0 есть два корня кратности 1 (x1=1, x2=-1)
        assertEquals(listOf(1.0, -1.0), mathFunc.solve(1.0, 0.0, -1.0))

        //для уравнения x^2+2x+1 = 0 есть один корень кратности 2 (x1= x2 = -1) (заменено согласно п.11)
        assertEquals(listOf(-1.0), mathFunc.solve(1.0, 2.0, 0.999999))

        //тест, который проверяет, что коэффициент a не может быть равен 0. В этом случае solve выбрасывает исключение.
        assertThrows(IllegalArgumentException::class.java) { mathFunc.solve(0.0, 2.0, 1.0) }

        //Посмотреть какие еще значения могут принимать числа типа double, кроме числовых и написать тест с их использованием на все коэффициенты. solve должен выбрасывать исключение.
        assertThrows(IllegalArgumentException::class.java) { mathFunc.solve(Double.NEGATIVE_INFINITY, 0.0, 0.0) }
        assertThrows(IllegalArgumentException::class.java) { mathFunc.solve(0.0, Double.NEGATIVE_INFINITY, 0.0) }
        assertThrows(IllegalArgumentException::class.java) { mathFunc.solve(0.0, 0.0, Double.NEGATIVE_INFINITY) }
        assertThrows(IllegalArgumentException::class.java) { mathFunc.solve(Double.POSITIVE_INFINITY, 0.0, 0.0) }
        assertThrows(IllegalArgumentException::class.java) { mathFunc.solve(0.0, Double.POSITIVE_INFINITY, 0.0) }
        assertThrows(IllegalArgumentException::class.java) { mathFunc.solve(0.0, 0.0, Double.POSITIVE_INFINITY) }
        assertThrows(IllegalArgumentException::class.java) { mathFunc.solve(Double.NaN, 0.0, 0.0) }
        assertThrows(IllegalArgumentException::class.java) { mathFunc.solve(0.0, Double.NaN, 0.0) }
        assertThrows(IllegalArgumentException::class.java) { mathFunc.solve(0.0, 0.0, Double.NaN) }
    }
}