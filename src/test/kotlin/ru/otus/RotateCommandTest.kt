package ru.otus

import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import java.util.NoSuchElementException

internal class RotateCommandTest {
    private val objProperties = mutableMapOf("direction" to 1, "angularVelocity" to 2, "maxDirections" to 7)
    private val slotInt = slot<Int>()
    private val slotString = slot<String>()
    private val obj: UObject = mockk()
    private val r: Rotable = RotableAdapter(r = obj)
    private val rotateCommand = RotateCommand(r = r)

    @BeforeEach
    fun setUp() {
        objProperties.clear()
        every { obj.getProperty(capture(slotString)) } answers { objProperties.getValue(slotString.captured) }
        every { obj.setProperty(capture(slotString), capture(slotInt)) } answers { objProperties[slotString.captured] = slotInt.captured }
    }

    //Для объекта, находящегося в направлении 1 и поворачивающего со скоростью 2, поворот меняет направление объекта на 3
    @Test
    fun rotate() {
        objProperties["direction"] = 1
        objProperties["angularVelocity"] = 2
        objProperties["maxDirections"] = 7
        rotateCommand.execute()
        assertEquals(3, obj.getProperty("direction"))
    }

    //Для объекта, находящегося в направлении 1, поворачивающего со скоростью 20 и имеющего максимальное значение поворота 7, поворот меняет направление объекта на 0
    @Test
    fun rotateOverMaxDirections() {
        objProperties["direction"] = 1
        objProperties["angularVelocity"] = 20
        objProperties["maxDirections"] = 7
        rotateCommand.execute()
        assertEquals(0, obj.getProperty("direction"))
    }

    //Попытка повернуть объект, у которого невозможно прочитать направление, приводит к ошибке
    @Test
    fun moveWithoutDirectionReed() {
        objProperties.remove("direction")
        assertThrows(NoSuchElementException::class.java) { rotateCommand.execute() }
        objProperties["direction"] = 0
    }

    //Попытка повернуть объект, у которого невозможно прочитать значение скорости поворота, приводит к ошибке
    @Test
    fun moveWithoutAngularVelocityReed() {
        objProperties.remove("angularVelocity")
        assertThrows(NoSuchElementException::class.java) { rotateCommand.execute() }
        objProperties["angularVelocity"] = 0
    }

    //Попытка повернуть объект, у которого невозможно изменить направление, приводит к ошибке
    @Test
    fun moveWithoutDirectionWrite() {
        every { obj.setProperty("direction", any()) } throws RuntimeException()
        assertThrows(RuntimeException::class.java) { rotateCommand.execute() }
    }
}