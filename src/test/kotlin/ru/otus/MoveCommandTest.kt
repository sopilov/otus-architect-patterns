package ru.otus

import io.mockk.every
import org.junit.jupiter.api.Test
import io.mockk.mockk
import io.mockk.slot

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import java.util.NoSuchElementException

internal class MoveCommandTest {
    private val objProperties = mutableMapOf<String, Vector>()
    private val slotVector = slot<Vector>()
    private val slotString = slot<String>()
    private val obj: UObject = mockk()
    private val m: Movable = MovableAdapter(m = obj)
    private val moveCommand = MoveCommand(m = m)

    @BeforeEach
    fun setUp() {
        objProperties.clear()
        every { obj.getProperty(capture(slotString)) } answers { objProperties.getValue(slotString.captured) }
        every { obj.setProperty(capture(slotString), capture(slotVector)) } answers { objProperties[slotString.captured] = slotVector.captured }
    }

    @Test
    fun move() {
        //Для объекта, находящегося в точке (12, 5) и движущегося со скоростью (-7, 3) движение меняет положение объекта на (5, 8)
        objProperties["position"] = Vector(x = 12, y = 5)
        objProperties["velocity"] = Vector(x = -7, y = 3)
        moveCommand.execute()
        assertEquals(Vector(x = 5, y = 8), obj.getProperty("position"))
    }

    //Попытка сдвинуть объект, у которого невозможно прочитать положение в пространстве, приводит к ошибке
    @Test
    fun moveWithoutPositionReed() {
        objProperties["velocity"] = Vector(x = -7, y = 3)
        assertThrows(NoSuchElementException::class.java) { moveCommand.execute() }
    }

    //Попытка сдвинуть объект, у которого невозможно прочитать значение мгновенной скорости, приводит к ошибке
    @Test
    fun moveWithoutVelocityReed() {
        objProperties["velocity"] = Vector(x = -7, y = 3)
        assertThrows(NoSuchElementException::class.java) { moveCommand.execute() }
    }

    //Попытка сдвинуть объект, у которого невозможно изменить положение в пространстве, приводит к ошибке
    @Test
    fun moveWithoutPositionWrite() {
        every { obj.setProperty("position", any()) } throws RuntimeException()
        assertThrows(RuntimeException::class.java) { moveCommand.execute() }
    }
}